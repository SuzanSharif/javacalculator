//package com.kenfogel.arithmeticparserv2;
package com.suzansharif.test;

//import com.kenfogel.arithmeticparserv2.exceptions.DivisionByZero;
//import com.kenfogel.arithmeticparserv2.exceptions.InvalidInputQueueString;
//import com.kenfogel.arithmeticparserv2.exceptions.NonBinaryExpression;
//import com.kenfogel.arithmeticparserv2.exceptions.NonMatchingParenthesis;
import com.suzansharif.evaluator.exception.DivisionByZero;
import com.suzansharif.evaluator.exception.InvalidInputQueueString;
import com.suzansharif.evaluator.exception.NonBinaryExpression;
import com.suzansharif.evaluator.exception.NonMatchingParenthesis;
import com.suzansharif.evaluator.business.ResultParserWithParenthesisImpl;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.fail;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Final assignment 420-517
 *
 * @author Ken Fogel
 */
@RunWith(Parameterized.class)
public class InvalidInputQueueStringTest {

    private ResultParserWithParenthesisImpl rp;
    private final Queue<String> infixQueue;

    /**
     * A static method is required to hold all the data to be tested and the
     * expected results for each test. This data must be stored in a
     * two-dimension array. The 'name' attribute of Parameters is a JUnit 4.11
     * feature
     *
     * @return The list of arrays
     */
    @Parameterized.Parameters(name = "{index} calculation[{0}]={1}]")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {new ArrayDeque<>(Arrays.asList("2", "+", "bob"))},
            {new ArrayDeque<>(Arrays.asList("*9", "/", "53"))},
            {new ArrayDeque<>(Arrays.asList("Four", "+", "Five"))},});
    }

    /**
     * Constructor that receives the parameters for testing
     *
     * @param infixQueue
     */
    public InvalidInputQueueStringTest(Queue<String> infixQueue) {
        this.infixQueue = infixQueue;
    }

    /**
     * Initialize the test object and the input queue
     */
    @Before
    public void setUp() {
        rp = new ResultParserWithParenthesisImpl();
    }

    /**
     * Conversion of infixQueue to postfixQueue should throw a
     * InvalidInputQueueString exception
     *
     * @throws NonMatchingParenthesis
     * @throws InvalidInputQueueString
     * @throws DivisionByZero
     * @throws NonBinaryExpression
     */
    @Test(expected = InvalidInputQueueString.class)
    public void testInvalidInputQueueString() throws NonMatchingParenthesis, InvalidInputQueueString, NonBinaryExpression, DivisionByZero {
        rp.convertInfixToPostfix(infixQueue);
        fail("InvalidInputQueueString was not thrown");
    }
}
