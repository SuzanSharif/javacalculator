package com.suzansharif.evaluator.business;

import com.suzansharif.evaluator.exception.DivisionByZero;
import com.suzansharif.evaluator.exception.InvalidInputQueueString;
import com.suzansharif.evaluator.exception.NonBinaryExpression;
import com.suzansharif.evaluator.exception.NonMatchingParenthesis;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Queue;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This Class to convert an infix expression to a postfix expression and then
 * evaluate the postfix expression. The input to the evaluator is a queue of
 * type String containing the infix expression and the output is a String
 * containing the result. The infix expression is made up of floating point
 * numbers and four operators. The operators are add(+), subtract(-),
 * multiply(*) and division(/). You will also implement parenthesis () on the
 * calculator.
 *
 * It handle the special case of expressions written as 4(3+2) or (3+2)(4+5)
 *
 *
 * @author Suzan Sharif
 * @version 2020-12-05
 */
public class ResultParserWithParenthesisImpl implements ResultParserWithParenthesis {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass().getName());
    private Queue<String> infixQueue;
    private Queue<String> postfixQueue;
    private Deque<String> operatorsStack;

    public ResultParserWithParenthesisImpl() {
        this.operatorsStack = new ArrayDeque<String>();
    }

    /**
     * Coverts infix expression to postfix expression
     *
     * @param infixQueue
     * @return postfixQueue
     * @throws NonMatchingParenthesis
     * @throws InvalidInputQueueString
     * @throws NonBinaryExpression
     */
    public Queue<String> convertInfixToPostfix(Queue<String> infixQueue) throws NonMatchingParenthesis, InvalidInputQueueString, NonBinaryExpression {
        this.postfixQueue = new ArrayDeque<String>();
        String previousValue = "";

        if (infixQueue != null || !infixQueue.isEmpty()) {
                            
            //modify the inputInfix and makes a new infixQueue if it has open close paranthesis without * befor or after it for cases like  4(3+2) or (3+2)(4+5)
            this.infixQueue = modifiedInfixQueue(infixQueue);
            if (countCloseParenthesis(this.infixQueue) != countOpenParenthesis(this.infixQueue)) {
                throw new NonMatchingParenthesis("Number of open and close parenthesis does not match");
            } else {
                while (!this.infixQueue.isEmpty()) { //until infixQueue gets empty
                    String currentValue = this.infixQueue.remove(); //gets head of  infix

                    if (!isOperand(currentValue) && !isOperator(currentValue) && !currentValue.equals("(") && !currentValue.equals(")")) {
                        // if  current value is not a number , not aparenthesis or operator, it throws an exception
                        throw new InvalidInputQueueString("Not a valid  Input,  Current value is not a number nore operator nor () : CurrentValue is " + currentValue);

                    } //if a number 
                    else if (isOperand(currentValue)) {
                        if (isOperand(previousValue)) {
                            //checking if previous value is a number , 2 numbers after eachother is not binary expression 
                            throw new NonBinaryExpression("Not a binary expression, There are 2 operands after each other: " + previousValue + "   " + currentValue);

                        } else {
                            this.postfixQueue.add(currentValue);
                            previousValue = currentValue;
                        }

                    } //if an operator 
                    else if (isOperator(currentValue)) {

                        if (this.infixQueue.isEmpty()) {//after operator there is nothing
                            throw new NonBinaryExpression("Not a binary expression, There is no element left into infix queue after this operator");
                        }
                        if (isOperator(previousValue)) {//two operator after each other
                            throw new NonBinaryExpression("Not a binary expression, There are two Operator after each other "  + previousValue + "   " + currentValue );
                        }

                        if (this.operatorsStack.isEmpty()) { //when Stack is empty  operator goes into stack
                            
                            this.operatorsStack.add(currentValue);
                            previousValue = currentValue;
                        } 
                        else {//if stack is not empty we have to scan it and check precedence
                            
                            String stackHead = this.operatorsStack.peek();
                        
                            if (getOperatorPriority(currentValue) > getOperatorPriority(stackHead)) {
                             
                                this.operatorsStack.push(currentValue);

                                previousValue = currentValue;
                            } 
                            
                            else if (getOperatorPriority(currentValue) <= getOperatorPriority(stackHead)) {
                              
                                if (!this.operatorsStack.isEmpty()) {
                                    //swipe 
                                  //  LOG.debug("current value " + currentValue + " stackHead  " + stackHead);
                                    String value = this.operatorsStack.remove();

                                    this.postfixQueue.add(value);
                                    this.operatorsStack.push(currentValue);
                                    previousValue = currentValue;

                                }
                            }
                        }
                    }//end of opertaor
                    
                    
                    // if current value is    "  (   "   
                    else if (currentValue.equals("(")) {
                        this.operatorsStack.push(currentValue);
                        previousValue = currentValue;
                    } 

                    //if current value is    "  )   "
                    else if (currentValue.equals(")")) {
                        String currentStack = this.operatorsStack.remove();
                        if (!isOperand(previousValue) && currentStack.equals("(")) {
                       
                            throw new NonBinaryExpression("This is not a binary experession, There is Emty close and ope paranthesis  ()  ");
                        }
                        while (!this.operatorsStack.isEmpty() && !currentStack.equals("(")) {

                            this.postfixQueue.add(currentStack);
                            currentStack = this.operatorsStack.remove();
                            previousValue = currentValue;
                        }
                    }

                }//end of while loop. infixqueue is empty 
            }

            while (!this.operatorsStack.isEmpty()) { // add all operators  aftre  emptying infixQueue from stack into postfix 
                if (!this.operatorsStack.element().equals("(")) { // skip open parenthesis and not adding it into postfix
                    this.postfixQueue.add(this.operatorsStack.remove());
                } else {
                    this.operatorsStack.remove();
                }
            }
        }//end of if null or empty
        else {
            throw new InvalidInputQueueString("This is an invalid input. It cannot be   null or empty");
        }

        return this.postfixQueue;

    }

    /**
     * Evaluates, postfix queue 
     *
     * @param postfixQueue
     * @return result
     * @throws DivisionByZero
     */
    public String evaluatePostfix(Queue<String> postfixQueue) throws DivisionByZero {
      
        Deque<String> operandStack = new ArrayDeque<>();
        double result = 0.0;
        String finalResult = "";
        while (!postfixQueue.isEmpty()) {
            String currentValue = postfixQueue.remove();
       //     LOG.debug("evaluatePostfix method Current value  : " + currentValue);
            if (isOperand(currentValue)) {
                operandStack.push(currentValue);

            } else if (isOperator(currentValue)) {
                if (!operandStack.isEmpty()) {
                    
                    String first = operandStack.remove();
                    String second = operandStack.remove();
                    result = calculateQueue(first, currentValue, second);
                    operandStack.push(result + "");

                }

            }

        }
        finalResult = result + "";
        LOG.debug("Final result is:    "  + finalResult);
        return finalResult;
    }
/**
 * Parses String values from Queue to Double and pass them to getResult method
 * 
 * @param first
 * @param operator
 * @param second
 * @return result
 * @throws DivisionByZero 
 */
    private double calculateQueue(String first, String operator, String second) throws DivisionByZero {
        double rightOperand = Double.parseDouble(first);
        double leftOperand = Double.parseDouble(second);
        double result = getResult(leftOperand, operator, rightOperand);
        return result;
    }

    /**
     * This method calculates the result between 2 operands and one operator
     *
     * @param leftOperand
     * @param operator
     * @param rightOperand
     * @return result
     * @throws DivisionByZero
     *
     */
    private double getResult(double leftOperand, String operator, double rightOperand) throws DivisionByZero {

        if (rightOperand == 0 && operator.equals("/")) {

            throw new DivisionByZero("Division by zero");

        }

        //Evaluate expression
        switch (operator) {

            case "+":
             
                return (leftOperand + rightOperand);

            case "-":
             
                return leftOperand - rightOperand;

            case "*":
            
                return leftOperand * rightOperand;

            case "/":
        
                return leftOperand / rightOperand;

            default:
                throw new DivisionByZero("Division by zero ");

        }
    }

    /**
     * Checks if infixQueue can be converted to numbers
     *
     * @param infix
     * @return true if it is numeric
     */
    private boolean isOperand(String infix) {
        boolean numeric = true;
        if (infix == null || infix.length() == 0) {
            numeric = false;
        }
        try {
            Double number = Double.parseDouble(infix);
        } catch (NumberFormatException ex) {
            numeric = false;
        }
        return numeric;
    }

    /**
     * checks if the character is a close parentheses
     *
     * @param str
     * @return true if operator
     */
    private boolean isOperator(String str) {
        return str.equals("+") || str.equals("-") || str.equals("/") || str.equals("*");
    }

    /**
     * *
     * This method reads the operator and if it's + or - it gives less priority
     * to it , compare to * or / (+ and -), (* and /) have same priority
     *
     * @param operator
     * @return priority
     */
    private int getOperatorPriority(String operator) {
        int priority = -1;
        if (operator.equals("+")) {
            priority = 0;
        }
        if (operator.equals("-")) {
            priority = 0;
        }
        if (operator.equals("*")) {
            priority = 1;
        }
        if (operator.equals("/")) {
            priority = 1;
        }
        return priority;
    }

    /**
     * Counts number of open parenthesis
     *
     * @param queue
     * @return counter
     */
    private int countOpenParenthesis(Queue<String> queue) {
        int counter = 0;
        counter = queue.stream().filter(s -> (s.equals("("))).map(_item -> 1).reduce(counter, Integer::sum);
        return counter;
    }

    /**
     * Counts close parenthesis
     *
     * @param queue
     * @return counter
     */
    private int countCloseParenthesis(Queue<String> queue) {
        int counter = 0;
        for (String s : queue) {
            if (s.equals(")")) {
                counter++;
            }
        }
        return counter;
    }

    /**
     * This method takes the inputInfix and modify it in a way that if between a
     * number and ( or ) it's empty it adds a
     * It handles cases like   -(1+2)     or   +(2+3)
     *
     * @param inputInfix
     * @return modifiedInfix
     */
    public Queue<String> modifiedInfixQueue(Queue<String> inputInfix) {

        String currentStr = "";
        String previousStr = null;
        Queue<String> modifiedInfix = new ArrayDeque();

        while (!inputInfix.isEmpty()) {
            currentStr = inputInfix.remove();
        //    LOG.debug("CurrentStr  " + currentStr);
            if (currentStr != null && previousStr != null) {

                if (currentStr.equals("(") && isOperand(previousStr)) {
                    modifiedInfix.add("*");
                    modifiedInfix.add(currentStr);
                    previousStr = currentStr;
               //     LOG.debug("previuse   " + previousStr);

                } else if (previousStr.equals(")") && isOperand(currentStr)) {
                    modifiedInfix.add("*");
                    modifiedInfix.add(currentStr);
                    previousStr = currentStr;

                } else if (previousStr.equals(")") && currentStr.equals("(")) {
                    modifiedInfix.add("*");
                    modifiedInfix.add(currentStr);
                    previousStr = currentStr;
                } else {
                    modifiedInfix.add(currentStr);
                    previousStr = currentStr;

                }
            } else {
                modifiedInfix.add(currentStr);
                previousStr = currentStr;
                if (currentStr.equals("-(")) {
                
                    modifiedInfix.remove();
                    modifiedInfix.add("-1");
                    modifiedInfix.add("*");
                    modifiedInfix.add("(");
                    currentStr = "(";
                    previousStr = currentStr;
                } else if (currentStr.equals("+(")) {
                    modifiedInfix.remove();
                    modifiedInfix.add("(");
                }

            }
        }
        return modifiedInfix;
    }

}//end class

