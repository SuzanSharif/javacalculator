package com.suzansharif.evaluator.business;

import com.suzansharif.evaluator.exception.DivisionByZero;
import com.suzansharif.evaluator.exception.InvalidInputQueueString;
import com.suzansharif.evaluator.exception.NonBinaryExpression;
import com.suzansharif.evaluator.exception.NonMatchingParenthesis;
import java.util.Queue;

/**
 *Interface for ResultParserWithParenthesis class 
 * @author suzan sharif
 * @version 2020-12-05
 */
public interface ResultParserWithParenthesis {

    public Queue<String> convertInfixToPostfix(Queue<String> infixQueue) throws NonMatchingParenthesis, InvalidInputQueueString, NonBinaryExpression;

    public String evaluatePostfix(Queue<String> postfixQueue) throws DivisionByZero;
}
