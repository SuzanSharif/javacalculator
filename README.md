# javaCalculator
Data Structures Lab: The Evaluator
Program converts an infix expression to a postfix expression and then evaluate the postfix expression. 
The input to the evaluator is a queue of type String containing the infix expression and the output is a String containing the result.
The infix expression is made up of floating point numbers and four operators. 
The operators are add(+), subtract(-), multiply(*) and division(/). You will also implement parenthesis () on the calculator.


